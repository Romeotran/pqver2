<?php
/**
 * 2002-2017 TemplateMonster
 *
 * TM Theme Installator
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2017 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

use Symfony\Component\Yaml\Yaml;
use PrestaShop\PrestaShop\Core\Addon\Theme\ThemeManagerBuilder;

class Tmthemeinstallator extends Module
{
    public function __construct()
    {
        $this->name = 'tmthemeinstallator';
        $this->tab = 'front_office_features';
        $this->version = '1.0.1';
        $this->bootstrap = true;
        $this->author = 'TemplateMonster';
        parent::__construct();
        $this->displayName = $this->l('TM Theme Installator');
        $this->description = $this->l('Module for packing theme.');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->theme_repository = (new ThemeManagerBuilder($this->context, Db::getInstance()))->buildRepository();
    }

    public function install()
    {
        $settings = $this->getModuleSettings();

        foreach ($settings as $name => $value) {
            Configuration::updateValue($name, $value);
        }

        return parent::install()
        && $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        $settings = $this->getModuleSettings();
        foreach (array_keys($settings) as $name) {
            Configuration::deleteByName($name);
        }

        return parent::uninstall();
    }

    /**
     * Array with all settings and default values
     * @return array $setting
     */
    protected function getModuleSettings()
    {
        $settings = array(
            'TM_THEME_INSTALLATOR_NAME' => 'classic',
            'TM_THEME_INSTALLATOR_DISPLAY_NAME' => 'Classic',
            'TM_THEME_INSTALLATOR_EMAIL' => '',
            'TM_THEME_INSTALLATOR_WEBSITE' => 'https://www.templatemonster.com',
            'TM_THEME_INSTALLATOR_AUTHOR' => 'TemplateMonster',
            'TM_THEME_INSTALLATOR_VERSION' => '1.0.0',
            'TM_THEME_INSTALLATOR_COMPATIBILITY_FROM' => '1.7.0.0',
            'TM_THEME_INSTALLATOR_COMPATIBILITY_TO' => ''
        );

        return $settings;
    }

    public function getContent()
    {
        $output = '';

        if (((bool)Tools::isSubmit('submitSettingModule')) == true) {
            if (!$errors = $this->validateSettings()) {
                $this->postProcess();
                $this->recurseCopy(_PS_THEME_DIR_, _PS_MODULE_DIR_.'tmthemeinstallator/upload');
                $this->writeYmlFile();
                $this->generateArchive();
            } else {
                $output .= $errors;
            }
        }

        $output .= $this->renderFormSettings();

        return $output;
    }

    public function getThemeHooks()
    {
        $src = array();
        $hooks = Hook::getHooks(false, false);

        foreach ($hooks as $key => $hook) {
            $hooks[$key]['modules'] = Hook::getModulesFromHook($hook['id_hook'], (int)Tools::getValue('show_modules'));
        }

        foreach ($hooks as $key => $hook) {
            foreach ($hook['modules'] as $module) {
                $src[$hook['name']][] = $module['name'];
            }
        }

        return $src;
    }

    public function getInfoImages()
    {
        $images = ImageTypeCore::getImagesTypes(null, false);
        $image_types = array();

        foreach ($images as $image) {
            $image_types[$image['name']] = array(
                'width' => (int)$image['width'],
                'height' => (int)$image['height'],
                'scope' =>
                    explode(
                        ',',
                        implode(
                            ",",
                            array_diff(
                                array(($image['products'] == 1 ? 'products' : ''),
                                ($image['categories'] == 1 ? 'categories' : ''),
                                ($image['manufacturers'] == 1 ? 'manufacturers' : ''),
                                ($image['suppliers'] == 1 ? 'suppliers' : ''),
                                ($image['stores'] == 1 ? 'stores' : '')),
                                array('')
                            )
                        )
                    )
            );
        }

        return $image_types;
    }

    private function generateArchive()
    {
        foreach ($_POST as $key => $value) {
            if (strncmp($key, 'modulesToExport_module', Tools::strlen('modulesToExport_module')) == 0) {
                $this->to_export[] = $value;
            }
        }

        $filepath =  _PS_MODULE_DIR_.'tmthemeinstallator/upload/';
        $zip = new ZipArchive();
        $zip_file_name = Configuration::get('TM_THEME_INSTALLATOR_NAME').'.zip';
        if ($zip->open(_PS_CACHE_DIR_.$zip_file_name, ZipArchive::OVERWRITE | ZipArchive::CREATE) === true) {
            $this->archiveThisFile($zip, Tools::getValue('theme_directory'), $filepath);

            foreach ($this->to_export as $row) {
                if (!in_array($row, $this->native_modules)) {
                    $this->archiveThisFile($zip, $row, _PS_ROOT_DIR_.'/modules/', 'dependencies/modules/');
                }
            }

            $zip->close();

            if (!is_file(_PS_CACHE_DIR_.$zip_file_name)) {
                $this->errors[] = Tools::displayError($this->module->l(sprintf('Could not create %1s', _PS_CACHE_DIR_.$zip_file_name)));
            }

            if (!$this->errors) {
                if (ob_get_length() > 0) {
                    ob_end_clean();
                }

                ob_start();
                header('Pragma: public');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Cache-Control: public');
                header('Content-Description: File Transfer');
                header('Content-type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$zip_file_name.'"');
                header('Content-Transfer-Encoding: binary');
                ob_end_flush();
                readfile(_PS_CACHE_DIR_.$zip_file_name);
                @unlink(_PS_CACHE_DIR_.$zip_file_name);
                exit;
            }
        }
    }

    /**
     * @param ZipArchive $obj
     * @param string $file
     * @param string $server_path
     * @param string $archive_path
     */
    private function archiveThisFile($obj, $file, $server_path, $archive_path)
    {
        if (is_dir($server_path.$file)) {
            $dir = scandir($server_path.$file);
            foreach ($dir as $row) {
                if ($row[0] != '.') {
                    $this->archiveThisFile($obj, $row, $server_path.$file.'/', $archive_path.$file.'/');
                }
            }
        } elseif (!$obj->addFile($server_path.$file, $archive_path.$file)) {
            $this->error = true;
        }
    }

    public function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        if (!is_dir($dst)) {
            @mkdir($dst);
        }

        while (false !== ($file = readdir($dir))) {
            if ($file != '.' && $file != '..' && $file != 'node_modules' && $file != 'package.json' && $file != '.idea') {
                if (is_dir($src.'/'.$file)) {
                    $this->recurseCopy($src.'/'.$file, $dst.'/'.$file);
                } else {
                    copy($src.'/'.$file, $dst.'/'.$file);
                }
            }
        }

        closedir($dir);
    }

    public function writeYmlFile()
    {
        $filepath =  _PS_MODULE_DIR_.'tmthemeinstallator/upload/config/theme.yml';
        $main = array(
            'name' => Configuration::get('TM_THEME_INSTALLATOR_NAME'),
            'display_name' => Configuration::get('TM_THEME_INSTALLATOR_DISPLAY_NAME'),
            'version' => Configuration::get('TM_THEME_INSTALLATOR_VERSION'),
            'author' => array(
                'name' => Configuration::get('TM_THEME_INSTALLATOR_AUTHOR'),
                'email' => Configuration::get('TM_THEME_INSTALLATOR_EMAIL'),
                'url' => Configuration::get('TM_THEME_INSTALLATOR_WEBSITE')
            ),
            'meta' => array(
                'compatibility' => array(
                    'from' => Configuration::get('TM_THEME_INSTALLATOR_COMPATIBILITY_FROM'),
                    'to' => Configuration::get('TM_THEME_INSTALLATOR_COMPATIBILITY_TO') == true ? Configuration::get('TM_THEME_INSTALLATOR_COMPATIBILITY_TO') : '~',
                ),
                'available_layouts' => array(
                    'layout-full-width' => array(
                        'name' => 'Full Width',
                        'description' => 'No side columns, ideal for distraction-free pages such as product pages.',
                    ),
                    'layout-both-columns' => array(
                        'name' => 'Three Columns',
                        'description' => 'One large central column and 2 side columns.',
                    ),
                    'layout-left-column' => array(
                        'name' => 'Two Columns, small left column',
                        'description' => 'Two columns with a small left column',
                    ),
                    'layout-right-column' => array(
                        'name' => 'Two Columns, small right column',
                        'description' => 'Two columns with a small right column',
                    )
                )
            )
        );

        $global_settings = array (
            'global_settings' => array(
                'configuration'  => array(
                    'PS_IMAGE_QUALITY' => Configuration::get('PS_IMAGE_QUALITY')
                ),
                'modules' => array(
                    'to_enable' => $this->getModules()
                ),
                'hooks' => array(
                    'modules_to_hook' => $this->getThemeHooks()
                ),
                'image_types' => $this->getInfoImages()
            ),
        );

        $theme_settings = array (
            'theme_settings' => array(
                'default_layout' => $this->getDefaultLayout(),
                'layouts' => $this->getLayouts()
            )
        );

        $dependencies = array (
            'dependencies' => array(
                'modules' => $this->getModules()
            ),
        );

        $yaml_main = Yaml::dump($main, 5, 2);
        $yaml_main = str_replace("'", "", "$yaml_main");

        $yaml_global_settings = Yaml::dump($global_settings, 5, 2);
        $yaml_theme_settings = Yaml::dump($theme_settings, 5, 2);
        $yaml_dependencies = Yaml::dump($dependencies, 5, 2);

        file_put_contents($filepath, $yaml_main."\n".$yaml_global_settings."\n".$yaml_theme_settings."\n".$yaml_dependencies);
    }

    public function getModules()
    {
        $modules = $this->getNativeModule();
        $folders = scandir(_PS_MODULE_DIR_);
        $name_folder = array();
        $array = array();

        foreach ($folders as $folder) {
            if ($folder != '.' && $folder != '..' && $folder != 'index.php' && $folder != '.htaccess') {
                $name_folder[] = $folder;
            }
        }

        foreach ($name_folder as $value) {
            if (!in_array($value, $modules)) {
                $module_active = Module::getInstanceByName($value);
                if (!empty($module_active) && $module_active->active) {
                    $array[] = $value;
                }
            }
        }

        return $array;
    }

    private function getNativeModule()
    {
        return array(
            'blockreassurance',
            'contactform',
            'dashactivity',
            'dashgoals',
            'dashproducts',
            'dashtrends',
            'followup',
            'graphnvd3',
            'gridhtml',
            'pagesnotfound',
            'ps_banner',
            'ps_categorytree',
            'ps_checkpayment',
            'ps_contactinfo',
            'ps_currencyselector',
            'ps_customeraccountlinks',
            'ps_customersignin',
            'ps_customtext',
            'ps_emailalerts',
            'ps_emailsubscription',
            'ps_facetedsearch',
            'ps_featuredproducts',
            'ps_imageslider',
            'ps_languageselector',
            'ps_legalcompliance',
            'ps_linklist',
            'ps_mainmenu',
            'ps_searchbar',
            'ps_sharebuttons',
            'ps_shoppingcart',
            'ps_socialfollow',
            'ps_wirepayment',
            'referralprogram',
            'sekeywords',
            'statsbestcategories',
            'statsbestcustomers',
            'statsbestmanufacturers',
            'statsbestproducts',
            'statsbestsuppliers',
            'statsbestvouchers',
            'statscarrier',
            'statscatalog',
            'statscheckup',
            'statsdata',
            'statsequipment',
            'statsforecast',
            'statslive',
            'statsnewsletter',
            'statsorigin',
            'statspersonalinfos',
            'statsproduct',
            'statsregistrations',
            'statssales',
            'statssearch',
            'statsstock',
            'statsvisits',
            'trackingfront',
            'welcome',
            'tmthemeinstallator'
        );
    }

    public function getLayouts()
    {
        $theme = $this->theme_repository->getInstanceByName($this->context->shop->theme->getName());
        $page_layouts = $theme->getPageLayouts();

        return $page_layouts;
    }

    public function getDefaultLayout()
    {
        $theme = $this->theme_repository->getInstanceByName($this->context->shop->theme->getName());
        $default_layout = $theme->getDefaultLayout();

        return $default_layout['key'];
    }

    /**
     * Build the module form
     * @return mixed
     */
    protected function renderFormSettings()
    {
        $fields_form = array(
            'form' => array(
                'tinymce' => false,
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Name'),
                        'name' => 'TM_THEME_INSTALLATOR_AUTHOR',
                        'required' => true,
                        'col' => 2,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Email'),
                        'name' => 'TM_THEME_INSTALLATOR_EMAIL',
                        'col' => 2,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Website'),
                        'name' => 'TM_THEME_INSTALLATOR_WEBSITE',
                        'required' => true,
                        'col' => 2,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Theme name'),
                        'name' => 'TM_THEME_INSTALLATOR_NAME',
                        'required' => true,
                        'col' => 2,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Theme display name'),
                        'name' => 'TM_THEME_INSTALLATOR_DISPLAY_NAME',
                        'required' => true,
                        'col' => 2,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Theme version'),
                        'name' => 'TM_THEME_INSTALLATOR_VERSION',
                        'required' => true,
                        'col' => 2,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Compatible from'),
                        'name' => 'TM_THEME_INSTALLATOR_COMPATIBILITY_FROM',
                        'required' => true,
                        'col' => 2,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Compatible to'),
                        'name' => 'TM_THEME_INSTALLATOR_COMPATIBILITY_TO',
                        'col' => 2,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Export current theme'),
                    'icon' => 'process-icon-export'
                ),
            )
        );

        $to_install = $this->getModules();
        $this->formatHelperArray($to_install);
        $fields_value = array();

        if (count($to_install) > 0) {
            foreach ($to_install as $module) {
                $fields_value['modulesToExport_module'.$module] = true;
            }

            $fields_form['form']['input'][] = array(
                'type' => 'checkbox',
                'class' => 'module_checkbox',
                'label' => $this->l('Select the theme\'s modules that you wish to export'),
                'values' => array(
                    'query' => $this->formatHelperArray($to_install),
                    'id' => 'id',
                    'name' => 'name',
                ),
                'name' => 'modulesToExport',
            );
        }



        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSettingModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValuesSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($fields_form));
    }

    /**
     * Validate filed values
     * @return array|bool errors or false if no errors
     */
    protected function validateSettings()
    {
        $errors = array();

        if (Tools::isEmpty(Tools::getValue('TM_THEME_INSTALLATOR_AUTHOR'))) {
            $errors[] = $this->l('Enter name');
        }

        if (Tools::isEmpty(Tools::getValue('TM_THEME_INSTALLATOR_WEBSITE'))) {
            $errors[] = $this->l('Enter website');
        }

        if (Tools::isEmpty(Tools::getValue('TM_THEME_INSTALLATOR_NAME'))) {
            $errors[] = $this->l('Enter theme directory');
        }

        if (Tools::isEmpty(Tools::getValue('TM_THEME_INSTALLATOR_VERSION'))) {
            $errors[] = $this->l('Enter theme version');
        }

        if (Tools::isEmpty(Tools::getValue('TM_THEME_INSTALLATOR_COMPATIBILITY_FROM'))) {
            $errors[] = $this->l('Enter compatible from');
        }


        if ($errors) {
            return $this->displayError(implode('<br />', $errors));
        } else {
            return false;
        }
    }

    private function formatHelperArray($origin_arr)
    {
        $fmt_arr = array();
        foreach ($origin_arr as $module) {
            $display_name = $module;
            $module_obj = Module::getInstanceByName($module);
            if (Validate::isLoadedObject($module_obj)) {
                $display_name = $module_obj->displayName;
            }
            $tmp = array();
            $tmp['id'] = 'module'.$module;
            $tmp['val'] = $module;
            $tmp['name'] = $display_name;
            $fmt_arr[] = $tmp;
        }

        return $fmt_arr;
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (Tools::getValue('configure') == $this->name) {
            $this->context->controller->addJquery();
            $this->context->controller->addJs($this->_path . 'views/js/tmthemeinstallator_admin.js');
        }
    }

    /**
     * Fill the module form values
     * @return array
     */
    protected function getConfigFormValuesSettings()
    {
        $filled_settings = array();
        $settings = $this->getModuleSettings();

        foreach (array_keys($settings) as $name) {
            $filled_settings[$name] = Configuration::get($name);
        }

        return $filled_settings;
    }

    /**
     * Update Configuration values
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValuesSettings();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }
}
