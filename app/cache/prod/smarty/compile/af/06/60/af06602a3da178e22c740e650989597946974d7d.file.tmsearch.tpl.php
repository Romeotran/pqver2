<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:09
         compiled from "D:\Project\PhuQuang\themes\theme1479\modules\tmsearch\\views\templates\hook\tmsearch.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18570270825a355cd10d8f36-76536929%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af06602a3da178e22c740e650989597946974d7d' => 
    array (
      0 => 'D:\\Project\\PhuQuang\\themes\\theme1479\\modules\\tmsearch\\\\views\\templates\\hook\\tmsearch.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18570270825a355cd10d8f36-76536929',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search_categories' => 0,
    'active_category' => 0,
    'category' => 0,
    'search_query' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd10e92d5_84455275',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd10e92d5_84455275')) {function content_5a355cd10e92d5_84455275($_smarty_tpl) {?>

<div id="tmsearchblock">
  <form id="tmsearchbox" method="get" action="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape(Tmsearch::getTMSearchLink('tmsearch'),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" >
    <?php if (!Configuration::get('PS_REWRITING_SETTINGS')) {?>
      <input type="hidden" name="fc" value="module" />
      <input type="hidden" name="controller" value="tmsearch" />
      <input type="hidden" name="module" value="tmsearch" />
    <?php }?>
    <div class="selector">
      <select name="search_categories" class="form-control">
        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['search_categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
          <option <?php if ($_smarty_tpl->tpl_vars['active_category']->value==$_smarty_tpl->tpl_vars['category']->value['id']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value['id'],'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['category']->value['id']==2) {?><?php echo smartyTranslate(array('s'=>'All Categories','mod'=>'tmsearch'),$_smarty_tpl);?>
<?php } else { ?><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['category']->value['name'],'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
<?php }?></option>
        <?php } ?>
      </select>
    </div>
    <input class="tm_search_query form-control" type="text" id="tm_search_query" name="search_query" placeholder="<?php echo smartyTranslate(array('s'=>'Search','mod'=>'tmsearch'),$_smarty_tpl);?>
" value="<?php echo htmlspecialchars(stripslashes($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['search_query']->value,'htmlall','UTF-8')), ENT_QUOTES, 'UTF-8');?>
" />
    <button type="submit" name="tm_submit_search" class="btn button-search">
      <span><?php echo smartyTranslate(array('s'=>'Search','mod'=>'tmsearch'),$_smarty_tpl);?>
</span>
    </button>
  </form>
</div>
<?php }} ?>
