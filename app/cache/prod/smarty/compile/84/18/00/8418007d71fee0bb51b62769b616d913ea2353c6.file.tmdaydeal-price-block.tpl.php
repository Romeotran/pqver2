<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:24
         compiled from "modules/tmdaydeal/views/templates/hook/tmdaydeal-price-block.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7813719055a3554ac0f06c0-08407289%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8418007d71fee0bb51b62769b616d913ea2353c6' => 
    array (
      0 => 'modules/tmdaydeal/views/templates/hook/tmdaydeal-price-block.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7813719055a3554ac0f06c0-08407289',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'daydeal_products_extra' => 0,
    'data_end' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554ac108099_82980703',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554ac108099_82980703')) {function content_5a3554ac108099_82980703($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['daydeal_products_extra']->value["data_end"])&&$_smarty_tpl->tpl_vars['daydeal_products_extra']->value["data_end"]) {?>
  <?php $_smarty_tpl->tpl_vars['data_end'] = new Smarty_variable($_smarty_tpl->tpl_vars['daydeal_products_extra']->value["data_end"], null, 0);?>

  <div class="block products_block daydeal-box">
    <h3><?php echo smartyTranslate(array('s'=>'Time left to buy','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</h3>
    <div data-countdown="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['data_end']->value,'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
"></div>
  </div>
  <script type="text/javascript">
    if (typeof(runTmDayDealCounter) != 'undefined') {
      runTmDayDealCounter();
    };
  </script>
<?php }?><?php }} ?>
