<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:24
         compiled from "modules/tmmegalayout//views/templates/hook/layouts/block.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7794226335a3554acb6e0c0-05910083%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fad3c58ef98e34552da6c6930c2252f8d628e8b9' => 
    array (
      0 => 'modules/tmmegalayout//views/templates/hook/layouts/block.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7794226335a3554acb6e0c0-05910083',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'urls' => 0,
    'shop' => 0,
    'HOOK_HOME_TAB_CONTENT' => 0,
    'HOOK_HOME_TAB' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554acba9d90_53117837',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554acba9d90_53117837')) {function content_5a3554acba9d90_53117837($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['items']->value['module_name']=="logo") {?>
  <div id="header_logo">
    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
">
      <img class="logo img-responsive" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
" />
    </a>
  </div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['items']->value['module_name']=="copyright") {?>
  <?php if (Configuration::get('FOOTER_POWEREDBY')) {?>
    <div class="bottom-footer">
      <?php echo smartyTranslate(array('s'=>'[1] %3$s %2$s - Ecommerce software by %1$s [/1]','mod'=>'tmmegalayout','sprintf'=>array('PrestaShop™',date('Y'),'©'),'tags'=>array('<a class="_blank" href="http://www.prestashop.com">')),$_smarty_tpl);?>

    </div>
  <?php }?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['items']->value['module_name']=="tabs") {?>
  <?php if (isset($_smarty_tpl->tpl_vars['HOOK_HOME_TAB_CONTENT']->value)&&trim($_smarty_tpl->tpl_vars['HOOK_HOME_TAB_CONTENT']->value)) {?>
    <?php if (isset($_smarty_tpl->tpl_vars['HOOK_HOME_TAB']->value)&&trim($_smarty_tpl->tpl_vars['HOOK_HOME_TAB']->value)) {?>
      <ul id="home-page-tabs" class="nav nav-tabs clearfix">
        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['HOOK_HOME_TAB']->value, ENT_QUOTES, 'UTF-8');?>

      </ul>
    <?php }?>
    <div class="tab-content"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['HOOK_HOME_TAB_CONTENT']->value, ENT_QUOTES, 'UTF-8');?>
</div>
  <?php }?>
<?php }?><?php }} ?>
