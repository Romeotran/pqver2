<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:09
         compiled from "D:\Project\PhuQuang\themes\theme1479\modules\tmheaderaccount\views\templates\hook\customer-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20715139825a355cd116e235-70310249%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7c9f49223b24434f8801f6bd4200301e77f93c4c' => 
    array (
      0 => 'D:\\Project\\PhuQuang\\themes\\theme1479\\modules\\tmheaderaccount\\views\\templates\\hook\\customer-account.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20715139825a355cd116e235-70310249',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'f_status' => 0,
    'link' => 0,
    'facebook_id' => 0,
    'g_status' => 0,
    'back' => 0,
    'google_id' => 0,
    'vk_status' => 0,
    'vkcom_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd118f6d7_88877422',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd118f6d7_88877422')) {function content_5a355cd118f6d7_88877422($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['f_status']->value) {?>
    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklink',array(),true), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'Facebook Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <span class="link-item">
        <i class="fa fa-facebook" aria-hidden="true"></i>
        <?php if (!$_smarty_tpl->tpl_vars['facebook_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With Facebook','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Facebook Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?>
      </span>
    </a>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['g_status']->value) {?>
    <a <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array(),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Google Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <span class="link-item">
        <i class="fa fa-google" aria-hidden="true"></i>
        <?php if (!$_smarty_tpl->tpl_vars['google_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With Google','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Google Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?>
      </span>
    </a>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['vk_status']->value) {?>
    <a <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array(),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'VK Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <span class="link-item">
        <i class="fa fa-vk" aria-hidden="true"></i>
        <?php if (!$_smarty_tpl->tpl_vars['vkcom_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With VK','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'VK Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?>
      </span>
    </a>
<?php }?>
<?php }} ?>
