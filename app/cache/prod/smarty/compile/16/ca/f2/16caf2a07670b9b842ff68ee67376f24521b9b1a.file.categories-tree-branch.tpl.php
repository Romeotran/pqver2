<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:25
         compiled from "/home/gian4143/public_html/modules/tmmegamenu/views/templates/hook/items/categories-tree-branch.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20996469425a3554add67d40-67665906%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '16caf2a07670b9b842ff68ee67376f24521b9b1a' => 
    array (
      0 => '/home/gian4143/public_html/modules/tmmegamenu/views/templates/hook/items/categories-tree-branch.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20996469425a3554add67d40-67665906',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'items' => 0,
    'page' => 0,
    'id_selected' => 0,
    'item' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554add99588_14196490',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554add99588_14196490')) {function content_5a3554add99588_14196490($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['items']->value)&&$_smarty_tpl->tpl_vars['items']->value) {?>
  <ul>
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
      <li class="category<?php if (isset($_smarty_tpl->tpl_vars['page']->value['page_name'])&&$_smarty_tpl->tpl_vars['page']->value['page_name']=='category'&&$_smarty_tpl->tpl_vars['id_selected']->value==$_smarty_tpl->tpl_vars['item']->value['id_category']) {?> sfHoverForce<?php }?>">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['item']->value['id_category']), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
        <?php if (isset($_smarty_tpl->tpl_vars['item']->value['children'])&&$_smarty_tpl->tpl_vars['item']->value['children']) {?>
          <?php echo $_smarty_tpl->getSubTemplate ('./categories-tree-branch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['item']->value['children'],'id_selected'=>$_smarty_tpl->tpl_vars['id_selected']->value), 0);?>

        <?php }?>
      </li>
    <?php } ?>
  </ul>
<?php }?><?php }} ?>
