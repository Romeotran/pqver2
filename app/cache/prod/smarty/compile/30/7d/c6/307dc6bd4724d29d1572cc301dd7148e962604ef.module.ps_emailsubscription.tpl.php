<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:27
         compiled from "module:ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4515600865a3554af039777-28249304%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:ps_emailsubscription/views/templates/hook/ps_emailsubscription.tpl',
      1 => 1513443732,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '4515600865a3554af039777-28249304',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'msg' => 0,
    'nw_error' => 0,
    'urls' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554af067106_74736313',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554af067106_74736313')) {function content_5a3554af067106_74736313($_smarty_tpl) {?>

<div class="block_newsletter">
  <h3 class="footer-heading"><?php echo smartyTranslate(array('s'=>'Newsletter','d'=>'Shop.Theme.Global'),$_smarty_tpl);?>
</h3>
  <?php if ($_smarty_tpl->tpl_vars['msg']->value) {?>
    <p class="alert <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>alert-danger<?php } else { ?>alert-success<?php }?>">
      <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>

    </p>
  <?php }?>
  <p id="block-newsletter-label"><?php echo smartyTranslate(array('s'=>'Get our latest news and special sales','d'=>'Shop.Theme.Global'),$_smarty_tpl);?>
</p>
  <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
#footer" method="post">
    <div class="input-wrapper">
      <input
        class="block-newsletter-label"
        name="email"
        type="text"
        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"
        placeholder="<?php echo smartyTranslate(array('s'=>'Your email address','d'=>'Shop.Forms.Labels'),$_smarty_tpl);?>
"
        aria-labelledby="block-newsletter-label"
      >
      <input
        class="submitNewsletter icon-newsletter material-icons"
        name="submitNewsletter"
        type="submit"
        value="email"
      >
    </div>
    <input type="hidden" name="action" value="0">
    <div class="clearfix"></div>
  </form>
</div>
<?php }} ?>
