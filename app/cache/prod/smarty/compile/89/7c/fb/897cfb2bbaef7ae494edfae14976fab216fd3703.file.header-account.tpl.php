<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:09
         compiled from "modules\tmheaderaccount\views\templates\hook\header-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5462442455a355cd13b5a96-37890490%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '897cfb2bbaef7ae494edfae14976fab216fd3703' => 
    array (
      0 => 'modules\\tmheaderaccount\\views\\templates\\hook\\header-account.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5462442455a355cd13b5a96-37890490',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'f_status' => 0,
    'g_status' => 0,
    'vk_status' => 0,
    'back' => 0,
    'back_page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd13e56c6_05269219',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd13e56c6_05269219')) {function content_5a355cd13e56c6_05269219($_smarty_tpl) {?>

<?php $_smarty_tpl->tpl_vars['back_page'] = new Smarty_variable($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('index'),'html','UTF-8'), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>
  <div class="clearfix social-login-buttons">
    <?php if ($_smarty_tpl->tpl_vars['f_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn btn-md btn-login-facebook" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklogin',array(),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklogin',array('back'=>$_smarty_tpl->tpl_vars['back_page']->value),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Login with Your Facebook Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Facebook Login','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['g_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['f_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn btn-md btn-login-google" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back_page']->value),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Login with Your Google Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Google Login','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['vk_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['f_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['f_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn btn-md btn-login-vk" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back_page']->value),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Login with Your VK Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'VK Login','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
  </div>
<?php }?><?php }} ?>
