<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:08
         compiled from "modules\tmproductlistgallery\views\templates\hook\tmproductlistgallery.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19650221465a355cd0b0a9b1-94457887%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '115ea0788aa0864f7f8e98d32e13dbd38a609cde' => 
    array (
      0 => 'modules\\tmproductlistgallery\\views\\templates\\hook\\tmproductlistgallery.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19650221465a355cd0b0a9b1-94457887',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'smarty_settings' => 0,
    'product_images' => 0,
    'product' => 0,
    'image' => 0,
    'imageId' => 0,
    'link' => 0,
    'imageTitle' => 0,
    'imageVisible' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd0b62301_22729668',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd0b62301_22729668')) {function content_5a355cd0b62301_22729668($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_display']) {?>
  <?php if (count($_smarty_tpl->tpl_vars['product_images']->value)>1) {?>
    <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
      <?php $_smarty_tpl->tpl_vars['imageId'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value['id_product'])."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
      <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
        <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['image']->value['legend'],'html','UTF-8'), null, 0);?>
      <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['product']->value['name'],'html','UTF-8'), null, 0);?>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='rollover') {?>
        <?php if ($_smarty_tpl->tpl_vars['image']->value['cover']!=1) {?>
          <img class="img-fluid hover-image" src="<?php if (Configuration::get('PS_SSL_ENABLED')) {?>https://<?php } else { ?>http://<?php }?><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" />
          <?php break 1?>
        <?php }?>
      <?php }?>
    <?php } ?>
    <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='slideshow') {?>
        <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']++;
?>
          <?php $_smarty_tpl->tpl_vars['imageId'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value['id_product'])."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
          <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
            <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['image']->value['legend'],'html','UTF-8'), null, 0);?>
          <?php } else { ?>
            <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['name'], null, 0);?>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['image']->value['cover']!=1) {?>
            <img class="img-fluid" src="<?php if (Configuration::get('PS_SSL_ENABLED')) {?>https://<?php } else { ?>http://<?php }?><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" />
          <?php }?>
        <?php } ?>
      <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_slider_controls']) {?>
        <p class="slideshow-controls"><em class="prev"></em><em class="next"></em></p>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_slider_pager']) {?>
        <p class="slideshow-pager">
          <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']++;
?>
            <em><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->getVariable('smarty')->value['foreach']['image']['iteration'],'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
</em>
          <?php } ?>
        </p>
      <?php }?>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_type']=='gallery') {?>
      <div class="gallery-wrapper">
        <ul class="gallery-thumb-list">
          <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product_images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['image']['iteration']++;
?>
            <?php $_smarty_tpl->tpl_vars['imageId'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['product']->value['id_product'])."-".((string)$_smarty_tpl->tpl_vars['image']->value['id_image']), null, 0);?>
            <?php if (!empty($_smarty_tpl->tpl_vars['image']->value['legend'])) {?>
              <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['image']->value['legend'],'html','UTF-8'), null, 0);?>
            <?php } else { ?>
              <?php $_smarty_tpl->tpl_vars['imageTitle'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['name'], null, 0);?>
            <?php }?>
            <?php $_smarty_tpl->tpl_vars['imageVisible'] = new Smarty_variable(100/$_smarty_tpl->tpl_vars['smarty_settings']->value['st_visible'], null, 0);?>
            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['image']['iteration']<$_smarty_tpl->tpl_vars['smarty_settings']->value['st_nb_items']+1) {?>
              <li id="thumb-<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['product']->value['id_product'],'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
-<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['image']->value['id_image'],'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" class="gallery-image-thumb<?php if ($_smarty_tpl->tpl_vars['image']->value['cover']==1) {?> active<?php }?><?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_gall_carousel']&&$_smarty_tpl->getVariable('smarty')->value['foreach']['image']['iteration']>$_smarty_tpl->tpl_vars['smarty_settings']->value['st_visible']) {?> clone-image<?php }?>" style="width: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['imageVisible']->value, ENT_QUOTES, 'UTF-8');?>
%;<?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_gall_carousel']&&$_smarty_tpl->getVariable('smarty')->value['foreach']['image']['iteration']>$_smarty_tpl->tpl_vars['smarty_settings']->value['st_visible']) {?> transform: translateX(<?php ob_start();?><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->getVariable('smarty')->value['foreach']['image']['iteration'],'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
<?php $_tmp1=ob_get_clean();?><?php echo htmlspecialchars($_tmp1-1, ENT_QUOTES, 'UTF-8');?>
00%)<?php }?>">
                <span data-href="<?php if (Configuration::get('PS_SSL_ENABLED')) {?>https://<?php } else { ?>http://<?php }?><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" data-title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" data-alt="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                  <img class="img-fluid" src="<?php if (Configuration::get('PS_SSL_ENABLED')) {?>https://<?php } else { ?>http://<?php }?><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value['link_rewrite'],$_smarty_tpl->tpl_vars['imageId']->value,'home_default'),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['imageTitle']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" itemprop="image" />
                </span>
              </li>
            <?php }?>
          <?php } ?>
        </ul>
        <?php if ($_smarty_tpl->tpl_vars['smarty_settings']->value['st_gall_carousel']&&count($_smarty_tpl->tpl_vars['product_images']->value)>$_smarty_tpl->tpl_vars['smarty_settings']->value['st_visible']) {?>
          <p class="gallery-controls"><span class="prev"></span><span class="next"></span></p>
        <?php }?>
      </div>
    <?php }?>
  <?php }?>
<?php }?>
<?php }} ?>
