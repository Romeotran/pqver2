<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:25
         compiled from "/home/gian4143/public_html/themes/theme1479/modules/tmheaderaccount//views/templates/hook/default/tmheaderaccount-content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20679386805a3554ad363e57-96621818%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ae370d495b3139e99663eedb9ed78d010c281f3' => 
    array (
      0 => '/home/gian4143/public_html/themes/theme1479/modules/tmheaderaccount//views/templates/hook/default/tmheaderaccount-content.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20679386805a3554ad363e57-96621818',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'customer' => 0,
    'hook' => 0,
    'configs' => 0,
    'avatar' => 0,
    'firstname' => 0,
    'lastname' => 0,
    'link' => 0,
    'returnAllowed' => 0,
    'voucherAllowed' => 0,
    'f_status' => 0,
    'facebook_id' => 0,
    'g_status' => 0,
    'back' => 0,
    'google_id' => 0,
    'vk_status' => 0,
    'vkcom_id' => 0,
    'login_form' => 0,
    'register_form' => 0,
    'HOOK_CREATE_ACCOUNT_FORM' => 0,
    'request_uri' => 0,
    'address' => 0,
    'countries' => 0,
    'vatnumber_ajax_call' => 0,
    'email_create' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554ad52d770_01183674',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554ad52d770_01183674')) {function content_5a3554ad52d770_01183674($_smarty_tpl) {?>



















<ul class="header-login-content toogle_content<?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?> is-logged<?php }?>">
  <?php if ($_smarty_tpl->tpl_vars['customer']->value['is_logged']) {?>
  <li <?php if ($_smarty_tpl->tpl_vars['hook']->value=='displayNav2') {?>class="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['configs']->value['TMHEADERACCOUNT_DISPLAY_STYLE'],'quotes','UTF-8'), ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
    <ul>
      <li class="user-data">
        <?php if ($_smarty_tpl->tpl_vars['configs']->value['TMHEADERACCOUNT_USE_AVATAR']) {?>
          <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['avatar']->value, ENT_QUOTES, 'UTF-8');?>
" alt=""<?php if ($_smarty_tpl->tpl_vars['hook']->value=='displayNav') {?> width="<?php if ($_smarty_tpl->tpl_vars['configs']->value['TMHEADERACCOUNT_DISPLAY_STYLE']=="twocolumns") {?>150<?php } else { ?>90<?php }?>"<?php }?>>
        <?php }?>
        <p><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['firstname']->value, ENT_QUOTES, 'UTF-8');?>
</span> <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['lastname']->value, ENT_QUOTES, 'UTF-8');?>
</span></p>
      </li>
      <li>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('history',true),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My orders','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
          <?php echo smartyTranslate(array('s'=>'My orders','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

        </a>
      </li>
      <?php if ($_smarty_tpl->tpl_vars['returnAllowed']->value) {?>
        <li>
          <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-follow',true),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My returns','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
            <?php echo smartyTranslate(array('s'=>'My merchandise returns','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </li>
      <?php }?>
      <li>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('order-slip',true),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My credit slips','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
          <?php echo smartyTranslate(array('s'=>'My credit slips','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

        </a>
      </li>
      <li>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('addresses',true),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My addresses','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
          <?php echo smartyTranslate(array('s'=>'My addresses','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

        </a>
      </li>
      <li>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('identity',true),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'Manage my personal information','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
          <?php echo smartyTranslate(array('s'=>'My personal info','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

        </a>
      </li>
      <?php if ($_smarty_tpl->tpl_vars['voucherAllowed']->value) {?>
        <li>
          <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('discount',true),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'My vouchers','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
            <?php echo smartyTranslate(array('s'=>'My vouchers','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </li>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['f_status']->value) {?>
        <li>
          <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklink',array(),true), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'Facebook Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
            <span class="link-item">
              <i class="fa fa-facebook" aria-hidden="true"></i>
              <?php if (!$_smarty_tpl->tpl_vars['facebook_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With Facebook','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Facebook Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?>
            </span>
          </a>
        </li>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['g_status']->value) {?>
        <li>
          <a <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array(),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Google Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
            <span class="link-item">
              <i class="fa fa-google" aria-hidden="true"></i>
              <?php if (!$_smarty_tpl->tpl_vars['google_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With Google','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'Google Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?>
            </span>
          </a>
        </li>
      <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['vk_status']->value) {?>
        <li>
          <a <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array(),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'VK Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
            <span class="link-item">
              <i class="fa fa-vk" aria-hidden="true"></i>
              <?php if (!$_smarty_tpl->tpl_vars['vkcom_id']->value) {?><?php echo smartyTranslate(array('s'=>'Connect With VK','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php } else { ?><?php echo smartyTranslate(array('s'=>'VK Login Manager','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
<?php }?>
            </span>
          </a>
        </li>
      <?php }?>
    </ul>
    <p class="logout">
      <a class="btn btn-default btn-lg btn-block" href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('index'),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
?mylogout" title="<?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
" rel="nofollow">
        <?php echo smartyTranslate(array('s'=>'Sign out','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

      </a>
    </p>
  </li>
  <?php } else { ?>
  <li class="login-content">
    <form action="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication',true),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" method="post">
      <div class="form_content clearfix">
        <?php $_smarty_tpl->tpl_vars["formFields"] = new Smarty_variable($_smarty_tpl->tpl_vars['login_form']->value['formFields'], null, 0);?>
        <?php echo $_smarty_tpl->getSubTemplate ("./_partials/form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        <p class="submit">
          <button type="submit" name="HeaderSubmitLogin" class="btn btn-primary btn-lg btn-block">
            <?php echo smartyTranslate(array('s'=>'Sign in','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </button>
        </p>
        <p>
          <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('my-account',true),'htmlall','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" class="create btn btn-default btn-lg btn-block">
            <?php echo smartyTranslate(array('s'=>'Create an account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </p>
        <p>
          <a href="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('password','true'),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" class="forgot-password btn btn-default btn-lg btn-block">
            <?php echo smartyTranslate(array('s'=>'Forgot your password?','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

          </a>
        </p>
        <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>"displayHeaderLoginButtons"),$_smarty_tpl);?>

      </div>
    </form>
  </li>
  <li class="create-account-content hidden">
    <div class="alert alert-danger" style="display:none;"></div>
    <form action="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication',true),'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" method="post" class="std">
      <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'HOOK_CREATE_ACCOUNT_TOP'),$_smarty_tpl);?>

      <div class="account_creation">
        <div class="clearfix">
          <?php $_smarty_tpl->tpl_vars["formFields"] = new Smarty_variable($_smarty_tpl->tpl_vars['register_form']->value['formFields'], null, 0);?>
          <?php echo $_smarty_tpl->getSubTemplate ("./_partials/form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        </div>
      </div>
      <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['HOOK_CREATE_ACCOUNT_FORM']->value, ENT_QUOTES, 'UTF-8');?>

      <div class="submit clearfix">
        <input type="hidden" name="email_create" value="1"/>
        <input type="hidden" name="is_new_customer" value="1"/>
        <input type="hidden" class="hidden" name="back" value="my-account"/>
        <p class="submit">
          <button type="submit" name="submitAccount" class="btn btn-default btn-lg btn-block">
            <span>
              <?php echo smartyTranslate(array('s'=>'Register','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

            </span>
          </button>
        </p>
        <p>
          <a href="#" class="btn btn-primary btn-lg btn-block signin"><span><?php echo smartyTranslate(array('s'=>'Sign in','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span></a>
        </p>
      </div>
    </form>
  </li>
  <li class="forgot-password-content hidden">
    <p><?php echo smartyTranslate(array('s'=>'Please enter the email address you used to register. We will then send you a new password. ','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</p>
    <form action="<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_smarty_tpl->tpl_vars['request_uri']->value,'html','UTF-8'), ENT_QUOTES, 'UTF-8');?>
" method="post" class="std">
      <fieldset>
        <div class="form-group">
          <div class="alert alert-success" style="display:none;"></div>
          <div class="alert alert-danger" style="display:none;"></div>
          <label for="email-forgot"><?php echo smartyTranslate(array('s'=>'Email address','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</label>
          <input class="form-control" type="email" name="email" id="email-forgot" value="<?php if (isset($_POST['email'])) {?><?php echo htmlspecialchars(stripslashes($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['escape'][0][0]->smartyEscape($_POST['email'],'html','UTF-8')), ENT_QUOTES, 'UTF-8');?>
<?php }?>"/>
        </div>
        <p class="submit">
          <button type="submit" class="btn btn-default btn-lg btn-block">
            <span>
              <?php echo smartyTranslate(array('s'=>'Retrieve Password','mod'=>'tmheaderaccount'),$_smarty_tpl);?>

            </span>
          </button>
        </p>
        <p>
          <a href="#" class="btn btn-primary btn-lg btn-block signin"><span><?php echo smartyTranslate(array('s'=>'Sign in','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span></a>
        </p>
      </fieldset>
    </form>
  </li>
    <script>
    <?php if (isset($_POST['id_state'])&&$_POST['id_state']) {?>idSelectedState = "<?php echo htmlspecialchars(intval($_POST['id_state']), ENT_QUOTES, 'UTF-8');?>
";<?php } elseif (isset($_smarty_tpl->tpl_vars['address']->value->id_state)&&$_smarty_tpl->tpl_vars['address']->value->id_state) {?>idSelectedState = "<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['address']->value->id_state), ENT_QUOTES, 'UTF-8');?>
";<?php } else { ?>idSelectedState = false;<?php }?><?php if (isset($_POST['id_state_invoice'])&&isset($_POST['id_state_invoice'])&&$_POST['id_state_invoice']) {?>idSelectedStateInvoice = "<?php echo htmlspecialchars(intval($_POST['id_state_invoice']), ENT_QUOTES, 'UTF-8');?>
";<?php } else { ?>idSelectedStateInvoice = false;<?php }?><?php if (isset($_POST['id_country'])&&$_POST['id_country']) {?>idSelectedCountry = "<?php echo htmlspecialchars(intval($_POST['id_country']), ENT_QUOTES, 'UTF-8');?>
";<?php } elseif (isset($_smarty_tpl->tpl_vars['address']->value->id_country)&&$_smarty_tpl->tpl_vars['address']->value->id_country) {?>idSelectedCountry = "<?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['address']->value->id_country), ENT_QUOTES, 'UTF-8');?>
";<?php } else { ?>idSelectedCountry = false;<?php }?><?php if (isset($_POST['id_country_invoice'])&&isset($_POST['id_country_invoice'])&&$_POST['id_country_invoice']) {?>idSelectedCountryInvoic = "<?php echo htmlspecialchars(intval($_POST['id_country_invoice']), ENT_QUOTES, 'UTF-8');?>
";<?php } else { ?>idSelectedCountryInvoic = false;<?php }?><?php if (isset($_smarty_tpl->tpl_vars['countries']->value)) {?>countries = <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['json_encode'][0][0]->jsonEncode($_smarty_tpl->tpl_vars['countries']->value);?>
;<?php }?><?php if (isset($_smarty_tpl->tpl_vars['vatnumber_ajax_call']->value)&&$_smarty_tpl->tpl_vars['vatnumber_ajax_call']->value) {?>vatnumber_ajax_call = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vatnumber_ajax_call']->value, ENT_QUOTES, 'UTF-8');?>
";<?php }?><?php if (isset($_smarty_tpl->tpl_vars['email_create']->value)&&$_smarty_tpl->tpl_vars['email_create']->value) {?>vatnumber_ajax_call = "<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['boolval'][0][0]->boolval($_smarty_tpl->tpl_vars['email_create']->value), ENT_QUOTES, 'UTF-8');?>
";<?php } else { ?>vatnumber_ajax_call = false;<?php }?>
  <?php }?>
    </script>
</ul><?php }} ?>
