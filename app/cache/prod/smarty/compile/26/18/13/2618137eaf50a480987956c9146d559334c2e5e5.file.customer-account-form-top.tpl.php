<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:24
         compiled from "modules/tmheaderaccount/views/templates/hook/customer-account-form-top.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9537889095a3554acc00c07-34522811%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2618137eaf50a480987956c9146d559334c2e5e5' => 
    array (
      0 => 'modules/tmheaderaccount/views/templates/hook/customer-account-form-top.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9537889095a3554acc00c07-34522811',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'f_status' => 0,
    'g_status' => 0,
    'vk_status' => 0,
    'link' => 0,
    'back' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554acc77260_41924159',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554acc77260_41924159')) {function content_5a3554acc77260_41924159($_smarty_tpl) {?>

<?php if ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>
  <div class="clearfix social-login-buttons">
    <?php if ($_smarty_tpl->tpl_vars['f_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn btn-md btn-login-facebook" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','facebooklogin',array(),true), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo smartyTranslate(array('s'=>'Register with Your Facebook Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Register with Your Facebook Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['g_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['f_status']->value&&$_smarty_tpl->tpl_vars['vk_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['f_status']->value||$_smarty_tpl->tpl_vars['vk_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn btn-md btn-login-google" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','googlelogin',array(),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Register with Your Google Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Register with Your Google Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['vk_status']->value) {?>
      <div class="<?php if ($_smarty_tpl->tpl_vars['g_status']->value&&$_smarty_tpl->tpl_vars['f_status']->value) {?>three-elements<?php } elseif ($_smarty_tpl->tpl_vars['g_status']->value||$_smarty_tpl->tpl_vars['f_status']->value) {?>two-elements<?php } else { ?>one-element<?php }?>">
        <a class="btn btn-md btn-login-vk" <?php if (isset($_smarty_tpl->tpl_vars['back']->value)&&$_smarty_tpl->tpl_vars['back']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array('back'=>$_smarty_tpl->tpl_vars['back']->value),true), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('tmheaderaccount','vklogin',array(),true), ENT_QUOTES, 'UTF-8');?>
"<?php }?> title="<?php echo smartyTranslate(array('s'=>'Register with Your VK Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
">
          <span><?php echo smartyTranslate(array('s'=>'Register with Your VK Account','mod'=>'tmheaderaccount'),$_smarty_tpl);?>
</span>
        </a>
      </div>
    <?php }?>
  </div>
<?php }?>
<?php }} ?>
