<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:24
         compiled from "/home/gian4143/public_html/themes/theme1479/modules/tmhtmlcontent/views/templates/hook/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18493109535a3554ac1c9349-17857478%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7135382afdf0bb1b1bdf471b3e258ee4ff1628c5' => 
    array (
      0 => '/home/gian4143/public_html/themes/theme1479/modules/tmhtmlcontent/views/templates/hook/home.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18493109535a3554ac1c9349-17857478',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'htmlitems' => 0,
    'hook' => 0,
    'hItem' => 0,
    'module_dir' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554ac23bdf5_58079787',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554ac23bdf5_58079787')) {function content_5a3554ac23bdf5_58079787($_smarty_tpl) {?><?php if (isset($_smarty_tpl->tpl_vars['htmlitems']->value)&&$_smarty_tpl->tpl_vars['htmlitems']->value) {?>
  <div id="tmhtmlcontent_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hook']->value, ENT_QUOTES, 'UTF-8');?>
">
    <ul class="tmhtmlcontent-home block-custom-secondary clearfix row">
      <?php  $_smarty_tpl->tpl_vars['hItem'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['hItem']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['htmlitems']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['items']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['hItem']->key => $_smarty_tpl->tpl_vars['hItem']->value) {
$_smarty_tpl->tpl_vars['hItem']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['items']['iteration']++;
?>
        <?php if ($_smarty_tpl->tpl_vars['hook']->value=='left'||$_smarty_tpl->tpl_vars['hook']->value=='right') {?>
          <li class="tmhtmlcontent-item-<?php echo htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['items']['iteration'], ENT_QUOTES, 'UTF-8');?>
 col-12">
            <div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hItem']->value['specific_class'], ENT_QUOTES, 'UTF-8');?>
">
        <?php } else { ?>
          <li class="tmhtmlcontent-item-<?php echo htmlspecialchars($_smarty_tpl->getVariable('smarty')->value['foreach']['items']['iteration'], ENT_QUOTES, 'UTF-8');?>
 col-12 col-md-6<?php if ((1 & $_smarty_tpl->getVariable('smarty')->value['foreach']['items']['iteration'])) {?> col-xl-9<?php } else { ?> col-xl-3<?php }?>">
            <div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hItem']->value['specific_class'], ENT_QUOTES, 'UTF-8');?>
">
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['hItem']->value['url']) {?>
          <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hItem']->value['url'], ENT_QUOTES, 'UTF-8');?>
" class="item-link"<?php if ($_smarty_tpl->tpl_vars['hItem']->value['target']==1) {?> onclick="return !window.open(this.href);"<?php }?> title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hItem']->value['title'], ENT_QUOTES, 'UTF-8');?>
">
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['hItem']->value['image']) {?>
          <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)$_smarty_tpl->tpl_vars['module_dir']->value)."img/".((string)$_smarty_tpl->tpl_vars['hItem']->value['image'])), ENT_QUOTES, 'UTF-8');?>
" class="item-img img-responsive" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hItem']->value['title'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hItem']->value['title'], ENT_QUOTES, 'UTF-8');?>
" width="<?php if ($_smarty_tpl->tpl_vars['hItem']->value['image_w']) {?><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['hItem']->value['image_w']), ENT_QUOTES, 'UTF-8');?>
<?php } else { ?>100%<?php }?>" height="<?php if ($_smarty_tpl->tpl_vars['hItem']->value['image_h']) {?><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['hItem']->value['image_h']), ENT_QUOTES, 'UTF-8');?>
<?php } else { ?>100%<?php }?>"/>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['hItem']->value['title']&&$_smarty_tpl->tpl_vars['hItem']->value['title_use']==1) {?>
          <h3 class="item-title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hItem']->value['title'], ENT_QUOTES, 'UTF-8');?>
</h3>
        <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['hItem']->value['html']) {?>
            <div class="item-html">
              <?php echo $_smarty_tpl->tpl_vars['hItem']->value['html'];?>

            </div>
          <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['hItem']->value['url']) {?>
          </a>
        <?php }?>
          </div>
        </li>
      <?php } ?>
    </ul>
  </div>
<?php }?>
<?php }} ?>
