<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:15:28
         compiled from "modules/tmmegamenu//views/templates/hook/tmmegamenu-script.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16265345515a3554b02d38e4-00035322%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '45eba24454dc204b08b52f5cc5f5595b656b0c10' => 
    array (
      0 => 'modules/tmmegamenu//views/templates/hook/tmmegamenu-script.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16265345515a3554b02d38e4-00035322',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'maps' => 0,
    'map' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a3554b03629b9_77034200',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a3554b03629b9_77034200')) {function content_5a3554b03629b9_77034200($_smarty_tpl) {?><script type="text/javascript">
  <?php  $_smarty_tpl->tpl_vars['map'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['map']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['maps']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['map']->key => $_smarty_tpl->tpl_vars['map']->value) {
$_smarty_tpl->tpl_vars['map']->_loop = true;
?>
    <?php if (isset($_smarty_tpl->tpl_vars['map']->value)&&$_smarty_tpl->tpl_vars['map']->value&&$_smarty_tpl->tpl_vars['map']->value['latitude']&&$_smarty_tpl->tpl_vars['map']->value['longitude']) {?>
      
        $(document).ready(function() {
          $('#tmmegamenu_map_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
').parents('.top-level-menu-li').one('mouseenter click', function() {
            setTimeout(function() {
              initMap<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
()
            }, 300)
          });
        });
        function initMap<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
() {
          var myLatLng<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
    = {
            lat : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
,
            lng : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
};
          var map_element<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
 = document.getElementById('tmmegamenu_map_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
');
          var image                                        = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['icon'], ENT_QUOTES, 'UTF-8');?>
';
          var map<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
         = new google.maps.Map(map_element<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
, {
            center                : myLatLng<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
,
            zoom                  : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['scale'], ENT_QUOTES, 'UTF-8');?>
,
            scrollwheel           : false,
            mapTypeControl        : false,
            streetViewControl     : false,
            draggable             : true,
            panControl            : false,
            mapTypeControlOptions : {
              style : google.maps.MapTypeControlStyle.DROPDOWN_MENU
            }
          });
          <?php if ($_smarty_tpl->tpl_vars['map']->value['icon']) {?>
          var marker<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
 = new google.maps.Marker({
            position : myLatLng<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
,
            map      : map<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
,
            icon     : image
          });
          <?php } else { ?>
          var marker<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
 = new google.maps.Marker({
            position : myLatLng<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
,
            map      : map<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['map']->value['id_item'], ENT_QUOTES, 'UTF-8');?>
,
          });
          <?php }?>
        }
      
    <?php }?>
  <?php } ?>
</script>
<?php }} ?>
