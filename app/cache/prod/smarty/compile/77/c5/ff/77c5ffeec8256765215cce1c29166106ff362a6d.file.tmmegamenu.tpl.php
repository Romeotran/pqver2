<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:09
         compiled from "modules\tmmegamenu\views\templates\hook\displaytop\tmmegamenu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4513858515a355cd171ab60-08300756%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '77c5ffeec8256765215cce1c29166106ff362a6d' => 
    array (
      0 => 'modules\\tmmegamenu\\views\\templates\\hook\\displaytop\\tmmegamenu.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4513858515a355cd171ab60-08300756',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'menu' => 0,
    'hook' => 0,
    'item' => 0,
    'id' => 0,
    'id_row' => 0,
    'row' => 0,
    'col' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd173ecb2_72900018',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd173ecb2_72900018')) {function content_5a355cd173ecb2_72900018($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['menu']->value)&&$_smarty_tpl->tpl_vars['menu']->value) {?>
  <div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hook']->value, ENT_QUOTES, 'UTF-8');?>
_menu top-level tmmegamenu_item default-menu top-global">
    <div class="menu-title tmmegamenu_item"><?php echo smartyTranslate(array('s'=>'Menu','mod'=>'tmmegamenu'),$_smarty_tpl);?>
</div>
    <ul class="menu clearfix top-level-menu tmmegamenu_item">
      <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['menu']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
        <li class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['specific_class'], ENT_QUOTES, 'UTF-8');?>
<?php if ($_smarty_tpl->tpl_vars['item']->value['is_simple']) {?> simple<?php }?> top-level-menu-li tmmegamenu_item <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8');?>
">
          <?php if ($_smarty_tpl->tpl_vars['item']->value['url']) {?>
            <a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8');?>
 top-level-menu-li-a tmmegamenu_item" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['url'], ENT_QUOTES, 'UTF-8');?>
">
          <?php } else { ?>
            <span class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8');?>
 top-level-menu-li-span tmmegamenu_item">
          <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['item']->value['title']) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8');?>
<?php }?>
              <?php if ($_smarty_tpl->tpl_vars['item']->value['badge']) {?>
                <span class="menu_badge <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8');?>
 top-level-badge tmmegamenu_item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['badge'], ENT_QUOTES, 'UTF-8');?>
</span>
              <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['item']->value['url']) {?>
            </a>
          <?php } else { ?>
            </span>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['item']->value['is_simple']) {?>
            <ul class="is-simplemenu tmmegamenu_item first-level-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8');?>
">
              <?php echo $_smarty_tpl->tpl_vars['item']->value['submenu'];?>

            </ul>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['item']->value['is_mega']) {?>
            <div class="is-megamenu tmmegamenu_item first-level-menu <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['unique_code'], ENT_QUOTES, 'UTF-8');?>
">
              <?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_smarty_tpl->tpl_vars['id_row'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value['submenu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
 $_smarty_tpl->tpl_vars['id_row']->value = $_smarty_tpl->tpl_vars['row']->key;
?>
                <div id="megamenu-row-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8');?>
" class="megamenu-row row megamenu-row-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8');?>
">
                  <?php if (isset($_smarty_tpl->tpl_vars['row']->value)) {?>
                    <?php  $_smarty_tpl->tpl_vars['col'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['col']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['row']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['col']->key => $_smarty_tpl->tpl_vars['col']->value) {
$_smarty_tpl->tpl_vars['col']->_loop = true;
?>
                      <div id="column-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8');?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['col'], ENT_QUOTES, 'UTF-8');?>
" class="megamenu-col megamenu-col-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_row']->value, ENT_QUOTES, 'UTF-8');?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['col'], ENT_QUOTES, 'UTF-8');?>
 col-sm-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['width'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['col']->value['class'], ENT_QUOTES, 'UTF-8');?>
">
                        <ul class="content">
                          <?php echo $_smarty_tpl->tpl_vars['col']->value['content'];?>

                        </ul>
                      </div>
                    <?php } ?>
                  <?php }?>
                </div>
              <?php } ?>
            </div>
          <?php }?>
        </li>
      <?php } ?>
    </ul>
  </div>
<?php }?><?php }} ?>
