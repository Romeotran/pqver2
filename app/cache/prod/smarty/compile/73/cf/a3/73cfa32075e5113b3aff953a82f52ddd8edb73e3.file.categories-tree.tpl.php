<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:09
         compiled from "modules\tmmegamenu\views\templates\hook\items\categories-tree.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5520783175a355cd147bbe2-57480224%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '73cfa32075e5113b3aff953a82f52ddd8edb73e3' => 
    array (
      0 => 'modules\\tmmegamenu\\views\\templates\\hook\\items\\categories-tree.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5520783175a355cd147bbe2-57480224',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tree' => 0,
    'page' => 0,
    'id_selected' => 0,
    'branch' => 0,
    'link' => 0,
    'url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd148dc16_96629912',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd148dc16_96629912')) {function content_5a355cd148dc16_96629912($_smarty_tpl) {?>

<?php if (isset($_smarty_tpl->tpl_vars['tree']->value)&&$_smarty_tpl->tpl_vars['tree']->value) {?>
  <?php  $_smarty_tpl->tpl_vars['branch'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['branch']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tree']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['branch']->key => $_smarty_tpl->tpl_vars['branch']->value) {
$_smarty_tpl->tpl_vars['branch']->_loop = true;
?>
    <li class="category<?php if (isset($_smarty_tpl->tpl_vars['page']->value['page_name'])&&$_smarty_tpl->tpl_vars['page']->value['page_name']=='category'&&$_smarty_tpl->tpl_vars['id_selected']->value==$_smarty_tpl->tpl_vars['branch']->value['id_category']) {?> sfHoverForce<?php }?>">
      <?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getPageLink('index'), null, 0);?>
      <?php if ($_smarty_tpl->tpl_vars['branch']->value['level_depth']>1) {?>
        <?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable($_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['branch']->value['id_category']), null, 0);?>
      <?php }?>
      <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['branch']->value['name'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['branch']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a>
      <?php if (isset($_smarty_tpl->tpl_vars['branch']->value['children'])&&$_smarty_tpl->tpl_vars['branch']->value['children']) {?>
        <?php echo $_smarty_tpl->getSubTemplate ('./categories-tree-branch.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['branch']->value['children'],'id_selected'=>$_smarty_tpl->tpl_vars['id_selected']->value), 0);?>

      <?php }?>
    </li>
  <?php } ?>
<?php }?><?php }} ?>
