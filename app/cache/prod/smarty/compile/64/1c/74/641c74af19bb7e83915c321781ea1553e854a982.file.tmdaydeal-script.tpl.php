<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:09
         compiled from "modules\tmdaydeal\\views\templates\hook\tmdaydeal-script.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17036514775a355cd1f3f8d7-41401122%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '641c74af19bb7e83915c321781ea1553e854a982' => 
    array (
      0 => 'modules\\tmdaydeal\\\\views\\templates\\hook\\tmdaydeal-script.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17036514775a355cd1f3f8d7-41401122',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd2002bc0_51717139',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd2002bc0_51717139')) {function content_5a355cd2002bc0_51717139($_smarty_tpl) {?>

<script type="text/javascript">
    var tmdd_msg_days = "<?php echo smartyTranslate(array('s'=>'days','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
    var tmdd_msg_hr = "<?php echo smartyTranslate(array('s'=>'hr','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
    var tmdd_msg_min = "<?php echo smartyTranslate(array('s'=>'min','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
    var tmdd_msg_sec = "<?php echo smartyTranslate(array('s'=>'sec','mod'=>'tmdaydeal','js'=>1),$_smarty_tpl);?>
";
    runTmDayDealCounter();
    function runTmDayDealCounter() {
        $("[data-countdown]").each(function() {
            var $this = $(this), finalDate = $(this).data("countdown");
            $this.countdown(finalDate, function(event) {
                $this.html(event.strftime('<span><span>%D</span>'+tmdd_msg_days+'</span><span><span>%H</span>'+tmdd_msg_hr+'</span><span><span>%M</span>'+tmdd_msg_min+'</span><span><span>%S</span>'+tmdd_msg_sec+'</span>'));
            });
        });
    }
</script><?php }} ?>
