<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:08
         compiled from "D:\Project\PhuQuang\themes\theme1479\modules\tmdaydeal\views\templates\hook\tmdaydeal-home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13440281335a355cd0d73332-02197432%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40886994f1448770c6d07c047a26b152a0bdee3f' => 
    array (
      0 => 'D:\\Project\\PhuQuang\\themes\\theme1479\\modules\\tmdaydeal\\views\\templates\\hook\\tmdaydeal-home.tpl',
      1 => 1513443732,
      2 => 'file',
    ),
    'd90f794f1a5727104a71db8243343681a7c15798' => 
    array (
      0 => 'D:\\Project\\PhuQuang\\themes\\theme1479\\templates\\catalog\\_partials\\variant-links.tpl',
      1 => 1513443733,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13440281335a355cd0d73332-02197432',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'daydeal_products' => 0,
    'product' => 0,
    'flag' => 0,
    'configuration' => 0,
    'urls' => 0,
    'static_token' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5a355cd0dbd580_64600481',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a355cd0dbd580_64600481')) {function content_5a355cd0dbd580_64600481($_smarty_tpl) {?>

<section id="products" class="daydeal-products clearfix">
  <h1 class="h1 products-section-title"><?php echo smartyTranslate(array('s'=>'Deal of the day','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</h1>
  <div class="products row">
    <?php if (isset($_smarty_tpl->tpl_vars['daydeal_products']->value)&&$_smarty_tpl->tpl_vars['daydeal_products']->value) {?>
      <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['daydeal_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
        <article class="product-miniature js-product-miniature" data-id-product="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['id_product'], ENT_QUOTES, 'UTF-8');?>
" data-id-product-attribute="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['id_product_attribute'], ENT_QUOTES, 'UTF-8');?>
" itemscope itemtype="http://schema.org/Product">
          <div class="thumbnail-container">
            
              <div class="product-thumbnail-container">
                <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['url'], ENT_QUOTES, 'UTF-8');?>
" class="thumbnail product-thumbnail">
                  <img
                    class="img-fluid"
                    src = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['cover']['bySize']['home_default']['url'], ENT_QUOTES, 'UTF-8');?>
"
                    alt = "<?php if (!empty($_smarty_tpl->tpl_vars['product']->value['info']['cover']['legend'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['cover']['legend'], ENT_QUOTES, 'UTF-8');?>
<?php } else { ?><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['info']['name'],30,'...'), ENT_QUOTES, 'UTF-8');?>
<?php }?>"
                    data-full-size-image-url = "<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['cover']['large']['url'], ENT_QUOTES, 'UTF-8');?>
"
                  >
                  <?php $_smarty_tpl->_capture_stack[0][] = array('displayProductListGallery', null, null); ob_start(); ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListGallery','product'=>$_smarty_tpl->tpl_vars['product']->value['info']),$_smarty_tpl);?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                  <?php if (Smarty::$_smarty_vars['capture']['displayProductListGallery']) {?>
                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductListGallery','product'=>$_smarty_tpl->tpl_vars['product']->value['info']),$_smarty_tpl);?>

                  <?php }?>
                </a>
              </div>
            
            
              <ul class="product-flags">
                <?php  $_smarty_tpl->tpl_vars['flag'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['flag']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['product']->value['info']['flags']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['flag']->key => $_smarty_tpl->tpl_vars['flag']->value) {
$_smarty_tpl->tpl_vars['flag']->_loop = true;
?>
                  <li class="product-flag <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['type'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flag']->value['label'], ENT_QUOTES, 'UTF-8');?>
</li>
                <?php } ?>
              </ul>
            
            <div class="product-description">
              
                <h1 class="h3 product-title product-name" itemprop="name"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['url'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['info']['name'],40,'...'), ENT_QUOTES, 'UTF-8');?>
</a></h1>
              
              
                <div class="product-short-description">
                  <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['info']['description_short'],140,'...');?>

                </div>
              
              
                <?php if ($_smarty_tpl->tpl_vars['product']->value['info']['show_price']) {?>
                  <div class="product-price-and-shipping">
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['info']['has_discount']) {?>
                      <span class="regular-price"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['regular_price'], ENT_QUOTES, 'UTF-8');?>
</span>
                      <?php if ($_smarty_tpl->tpl_vars['product']->value['info']['discount_type']==='percentage') {?>
                        <span class="price discount-percentage"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['discount_percentage'], ENT_QUOTES, 'UTF-8');?>
</span>
                      <?php }?>
                    <?php }?>
                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value['info'],'type'=>"before_price"),$_smarty_tpl);?>

                    <span itemprop="price" class="price<?php if ($_smarty_tpl->tpl_vars['product']->value['info']['has_discount']) {?> has_discount<?php }?>"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['price'], ENT_QUOTES, 'UTF-8');?>
</span>
                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value['info'],'type'=>'unit_price'),$_smarty_tpl);?>

                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value['info'],'type'=>'weight'),$_smarty_tpl);?>

                  </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['product']->value['info']['show_price']) {?>
                  <?php if ($_smarty_tpl->tpl_vars['product']->value['info']['has_discount']) {?>
                    <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0][0]->smartyHook(array('h'=>'displayProductPriceBlock','product'=>$_smarty_tpl->tpl_vars['product']->value['info'],'type'=>"old_price"),$_smarty_tpl);?>

                    <div class="daydeal-toggle hidden-md-up"><i class="material-icons">&#xE855;</i><i class="material-icons clear">&#xE14C;</i></div>
                  <?php }?>
                <?php }?>
              
            </div>

            <div class="buttons-wrap">
              
                <?php if (!$_smarty_tpl->tpl_vars['configuration']->value['info']['is_catalog']&&$_smarty_tpl->tpl_vars['product']->value['info']['minimal_quantity']<$_smarty_tpl->tpl_vars['product']->value['info']['quantity']) {?>
                  <div class="product-actions">
                    <form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['cart'], ENT_QUOTES, 'UTF-8');?>
" method="post">
                      <div class="product-quantity" style="display:none;">
                        <input type="hidden" name="token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <input type="hidden" name="id_product" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['id_product'], ENT_QUOTES, 'UTF-8');?>
">
                        <input type="hidden" name="id_customization" value="0">
                        <input type="number" name="qty" value="1" class="input-group"  min="1"  />
                      </div>
                      <?php if ($_smarty_tpl->tpl_vars['product']->value['info']['customizable']==0) {?>
                        <a href="javascript:void(0);" class="ajax_add_to_cart_button btn btn-primary add-to-cart" data-button-action="add-to-cart" title="<?php echo smartyTranslate(array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>
" >
                          <i class="material-icons shopping-cart">shopping_cart</i><span><?php echo smartyTranslate(array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>
</span>
                        </a>
                      <?php } else { ?>
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['info']['url'], ENT_QUOTES, 'UTF-8');?>
" class="btn btn-primary customize" title="<?php echo smartyTranslate(array('s'=>'Customize','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>
">
                          <i class="material-icons settings">settings</i><span><?php echo smartyTranslate(array('s'=>'Customize','d'=>'Shop.Theme.Actions'),$_smarty_tpl);?>
</span></a>
                      <?php }?>
                    </form>
                  </div>
                <?php }?>
              
              
                <a href="#" class="btn btn-primary-lighter quick-view" data-link-action="quickview">
                  <i class="material-icons">visibility</i><span><?php echo smartyTranslate(array('s'=>'Quick view','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</span>
                </a>
              
            </div>
            <div class="highlighted-informations<?php if (!$_smarty_tpl->tpl_vars['product']->value['info']['main_variants']) {?> no-variants<?php }?> hidden">
              
                <?php if ($_smarty_tpl->tpl_vars['product']->value['info']['main_variants']) {?>
                  <?php /*  Call merged included template "catalog/_partials/variant-links.tpl" */
$_tpl_stack[] = $_smarty_tpl;
 $_smarty_tpl = $_smarty_tpl->setupInlineSubTemplate('catalog/_partials/variant-links.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('variants'=>$_smarty_tpl->tpl_vars['product']->value['info']['main_variants']), 0, '13440281335a355cd0d73332-02197432');
content_5a355cd0db0e68_40999218($_smarty_tpl);
$_smarty_tpl = array_pop($_tpl_stack); 
/*  End of included template "catalog/_partials/variant-links.tpl" */?>
                <?php }?>
              
            </div>
           </div>
        </article>
      <?php } ?>
    <?php } else { ?>
      <p class="alert alert-info"><?php echo smartyTranslate(array('s'=>'No special products at this time.','mod'=>'tmdaydeal'),$_smarty_tpl);?>
</p>
    <?php }?>
  </div>
</section><?php }} ?>
<?php /* Smarty version Smarty-3.1.19, created on 2017-12-16 12:50:08
         compiled from "D:\Project\PhuQuang\themes\theme1479\templates\catalog\_partials\variant-links.tpl" */ ?>
<?php if ($_valid && !is_callable('content_5a355cd0db0e68_40999218')) {function content_5a355cd0db0e68_40999218($_smarty_tpl) {?><div class="variant-links">
  <?php  $_smarty_tpl->tpl_vars['variant'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['variant']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['variants']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['variant']->key => $_smarty_tpl->tpl_vars['variant']->value) {
$_smarty_tpl->tpl_vars['variant']->_loop = true;
?>
    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['variant']->value['url'], ENT_QUOTES, 'UTF-8');?>
"
       class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['variant']->value['type'], ENT_QUOTES, 'UTF-8');?>
"
       title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['variant']->value['name'], ENT_QUOTES, 'UTF-8');?>
"
       
      <?php if ($_smarty_tpl->tpl_vars['variant']->value['html_color_code']) {?> style="background-color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['variant']->value['html_color_code'], ENT_QUOTES, 'UTF-8');?>
" <?php }?>
      <?php if ($_smarty_tpl->tpl_vars['variant']->value['texture']) {?> style="background-image: url(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['variant']->value['texture'], ENT_QUOTES, 'UTF-8');?>
)" <?php }?>
    ><span class="sr-only"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['variant']->value['name'], ENT_QUOTES, 'UTF-8');?>
</span></a>
  <?php } ?>
  <span class="js-count count"></span>
</div>
<?php }} ?>
