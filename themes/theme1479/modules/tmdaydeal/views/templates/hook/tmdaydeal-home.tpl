{*
* 2002-2017 TemplateMonster
*
* TemplateMonster Deal of Day
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster (Sergiy Sakun)
*  @copyright 2002-2017 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<section id="products" class="daydeal-products clearfix">
  <h1 class="h1 products-section-title">{l s='Deal of the day' mod='tmdaydeal'}</h1>
  <div class="products row">
    {if isset($daydeal_products) && $daydeal_products}
      {foreach from=$daydeal_products item=product name=product}
        <article class="product-miniature js-product-miniature" data-id-product="{$product.info.id_product}" data-id-product-attribute="{$product.info.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
          <div class="thumbnail-container">
            {block name='product_thumbnail'}
              <div class="product-thumbnail-container">
                <a href="{$product.info.url}" class="thumbnail product-thumbnail">
                  <img
                    class="img-fluid"
                    src = "{$product.info.cover.bySize.home_default.url}"
                    alt = "{if !empty($product.info.cover.legend)}{$product.info.cover.legend}{else}{$product.info.name|truncate:30:'...'}{/if}"
                    data-full-size-image-url = "{$product.info.cover.large.url}"
                  >
                  {capture name='displayProductListGallery'}{hook h='displayProductListGallery' product=$product.info}{/capture}
                  {if $smarty.capture.displayProductListGallery}
                    {hook h='displayProductListGallery' product=$product.info}
                  {/if}
                </a>
              </div>
            {/block}
            {block name='product_flags'}
              <ul class="product-flags">
                {foreach from=$product.info.flags item=flag}
                  <li class="product-flag {$flag.type}">{$flag.label}</li>
                {/foreach}
              </ul>
            {/block}
            <div class="product-description">
              {block name='product_name'}
                <h1 class="h3 product-title product-name" itemprop="name"><a href="{$product.info.url}">{$product.info.name|truncate:40:'...'}</a></h1>
              {/block}
              {block name='product_description_short'}
                <div class="product-short-description">
                  {$product.info.description_short|truncate:140:'...' nofilter}
                </div>
              {/block}
              {block name='product_price_and_shipping'}
                {if $product.info.show_price}
                  <div class="product-price-and-shipping">
                    {if $product.info.has_discount}
                      <span class="regular-price">{$product.info.regular_price}</span>
                      {if $product.info.discount_type === 'percentage'}
                        <span class="price discount-percentage">{$product.info.discount_percentage}</span>
                      {/if}
                    {/if}
                    {hook h='displayProductPriceBlock' product=$product.info type="before_price"}
                    <span itemprop="price" class="price{if $product.info.has_discount} has_discount{/if}">{$product.info.price}</span>
                    {hook h='displayProductPriceBlock' product=$product.info type='unit_price'}
                    {hook h='displayProductPriceBlock' product=$product.info type='weight'}
                  </div>
                {/if}
                {if $product.info.show_price}
                  {if $product.info.has_discount}
                    {hook h='displayProductPriceBlock' product=$product.info type="old_price"}
                    <div class="daydeal-toggle hidden-md-up"><i class="material-icons">&#xE855;</i><i class="material-icons clear">&#xE14C;</i></div>
                  {/if}
                {/if}
              {/block}
            </div>

            <div class="buttons-wrap">
              {block name='product_buy'}
                {if !$configuration.info.is_catalog && $product.info.minimal_quantity < $product.info.quantity}
                  <div class="product-actions">
                    <form action="{$urls.pages.cart}" method="post">
                      <div class="product-quantity" style="display:none;">
                        <input type="hidden" name="token" value="{$static_token}">
                        <input type="hidden" name="id_product" value="{$product.info.id_product}">
                        <input type="hidden" name="id_customization" value="0">
                        <input type="number" name="qty" value="1" class="input-group"  min="1"  />
                      </div>
                      {if $product.info.customizable == 0}
                        <a href="javascript:void(0);" class="ajax_add_to_cart_button btn btn-primary add-to-cart" data-button-action="add-to-cart" title="{l s='Add to cart' d='Shop.Theme.Actions'}" >
                          <i class="material-icons shopping-cart">shopping_cart</i><span>{l s='Add to cart' d='Shop.Theme.Actions'}</span>
                        </a>
                      {else}
                        <a href="{$product.info.url}" class="btn btn-primary customize" title="{l s='Customize' d='Shop.Theme.Actions'}">
                          <i class="material-icons settings">settings</i><span>{l s='Customize' d='Shop.Theme.Actions'}</span></a>
                      {/if}
                    </form>
                  </div>
                {/if}
              {/block}
              {block name='quick_view'}
                <a href="#" class="btn btn-primary-lighter quick-view" data-link-action="quickview">
                  <i class="material-icons">visibility</i><span>{l s='Quick view' mod='tmdaydeal'}</span>
                </a>
              {/block}
            </div>
            <div class="highlighted-informations{if !$product.info.main_variants} no-variants{/if} hidden">
              {block name='product_variants'}
                {if $product.info.main_variants}
                  {include file='catalog/_partials/variant-links.tpl' variants=$product.info.main_variants}
                {/if}
              {/block}
            </div>
           </div>
        </article>
      {/foreach}
    {else}
      <p class="alert alert-info">{l s='No special products at this time.' mod='tmdaydeal'}</p>
    {/if}
  </div>
</section>