{if isset($htmlitems) && $htmlitems}
  <div id="tmhtmlcontent_{$hook}">
    <ul class="tmhtmlcontent-home block-custom-secondary clearfix row">
      {foreach name=items from=$htmlitems item=hItem}
        {if $hook == 'left' || $hook == 'right'}
          <li class="tmhtmlcontent-item-{$smarty.foreach.items.iteration} col-12">
            <div class="{$hItem.specific_class}">
        {else}
          <li class="tmhtmlcontent-item-{$smarty.foreach.items.iteration} col-12 col-md-6{if $smarty.foreach.items.iteration is not even} col-xl-9{else} col-xl-3{/if}">
            <div class="{$hItem.specific_class}">
        {/if}
        {if $hItem.url}
          <a href="{$hItem.url}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title}">
        {/if}
        {if $hItem.image}
          <img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img img-responsive" title="{$hItem.title}" alt="{$hItem.title}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
        {/if}
        {if $hItem.title && $hItem.title_use == 1}
          <h3 class="item-title">{$hItem.title}</h3>
        {/if}
          {if $hItem.html}
            <div class="item-html">
              {$hItem.html nofilter}
            </div>
          {/if}
        {if $hItem.url}
          </a>
        {/if}
          </div>
        </li>
      {/foreach}
    </ul>
  </div>
{/if}
