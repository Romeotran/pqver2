{assign var=_counter value=0}
{if $page.page_name == "index"}
  {function name="menu" nodes=[] depth=0 parent=null}
    {if $nodes|count}
      <ul class="top-menu" {if $depth == 0}id="top-menu"{/if} data-depth="{$depth}">
        {foreach from=$nodes item=node}
          <li class="{$node.type}{if $node.current} current {/if}" id="{$node.page_identifier}">
            {assign var=_counter value=$_counter+1}
            <a
              class="{if $depth >= 0}dropdown-item{/if}{if $depth === 1} dropdown-submenu{/if}"
              href="{$node.url}" data-depth="{$depth}"
              {if $node.open_in_new_window} target="_blank" {/if}
            >{$node.label}</a>
            {if $depth > 0}
              {if $node.children|count}
                {* Cannot use page identifier as we can have the same page several times *}
                {assign var=_expand_id value=10|mt_rand:100000}
                <span class="hidden-md-up submenu-toggle material-icons add">&#xE313;</span>
              {/if}
            {/if}
            {if $node.children|count}
              <div {if $depth > 0} class="popover sub-menu js-sub-menu collapse"{else} class="collapse"{/if} id="top_sub_menu_{$_expand_id}">
                {menu nodes=$node.children depth=$node.depth parent=$node}
              </div>
            {/if}
          </li>
        {/foreach}
      </ul>
    {/if}
  {/function}

  <div class="menu js-top-menu position-static">
    {menu nodes=$menu.children}
    <div class="clearfix"></div>
  </div>
{/if}