/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

$(document).ready(() => {
  var $testimonials = $('#cms #testimonials');
  if ($testimonials.length) {
    $testimonials.slick({
      infinite: false,
      slidesToShow: 1,
      arrows: true,
      prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" role="button" aria-disabled="false"><i class="fa fa-angle-left"></i></button>',
      nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" role="button" aria-disabled="false"><i class="fa fa-angle-right"></i></button>',
      dots: false,
      slidesToScroll: 1
    });
  }
  sitemapAccordion();
  counter();
});


function sitemapAccordion() {
  $('#sitemap #content-wrapper ul.tree > li > ul')
    .addClass('accordion_content')
    .parent()
    .find('> a')
    .wrap('<p class="page-subheading accordion_current"></p>');

  $('#content-wrapper .accordion_current').on('click', function() {
    $(this)
      .toggleClass('active')
      .parent()
      .find('.accordion_content')
      .stop()
      .slideToggle('medium');
  });

  $('#content-wrapper')
    .addClass('accordionBox')
    .find('.accordion_content')
    .slideUp('fast');

  if (typeof(ajaxCart) !== 'undefined') {
    ajaxCart.collapse();
  }
}

function counter() {
  $('.count').each(function() {
    $(this).prop('Counter',0).animate({
      Counter: $(this).text()
    }, {
      duration: 4000,
      easing: 'swing',
      step: function(now) {
        $(this).text(Math.ceil(now));
      }
    });
  });
}