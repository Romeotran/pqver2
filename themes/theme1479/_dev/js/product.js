/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import $ from 'jquery';

$(document).ready(function () {
  createProductSpin();
  createInputFile();
  coverImage();
  imageScrollBox();
  refreshImages();
  productAccessoriesCarousel();
  $('body').on(
    'click',
    '.product-refresh',
    function (event, extraParameters) {
      var $productRefresh = $(this);
      event.preventDefault();

      let eventType = 'updatedProductCombination';
      if (typeof extraParameters !== 'undefined' && extraParameters.eventType) {
        eventType = extraParameters.eventType;
      }

      var query = $(event.target.form).serialize() + '&ajax=1&action=productrefresh';
      var actionURL = $(event.target.form).attr('action');

      $.post(actionURL, query, null, 'json').then(function(resp) {
        prestashop.emit('updateProduct', {
          reason: {
            productUrl: resp.productUrl
          },
          refreshUrl: $productRefresh.data('url-update'),
          eventType: eventType
        });
      });
    }
  );

  prestashop.on('updatedProduct', function (event) {
    createInputFile();
    var quickViewProductImages = $('.quickview .js-qv-product-images');
    if (quickViewProductImages.not(".slick-slider").length && quickViewProductImages.find('li').length > 2) {
      quickViewProductImages.not(".slick-slider").slick({
        slidesToShow: 4,
        infinite: false,
        slidesToScroll: 1,
        dots: false,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" role="button" aria-disabled="false"><i class="fa fa-angle-left"></i></button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" role="button" aria-disabled="false"><i class="fa fa-angle-right"></i></button>',
        responsive: [{
          breakpoint: 991,
          settings: {
            slidesToShow: 3
          }
        }]
      });
    }

    if ($('select').not(".select-custom-activated").length){
      const select = $('select');
      select.each(function () {
        var div = document.createElement('div');
        var span = document.createElement('span');
        div.classList.add("select-custom");
        span.classList.add("select-custom-arrow");
        this.parentNode.insertBefore(div, this);
        div.appendChild(this);
        div.appendChild(span);
        this.classList.add("select-custom-activated");
      })
    }

    if (event && event.product_minimal_quantity) {
      const minimalProductQuantity = parseInt(event.product_minimal_quantity, 10);
      const quantityInputSelector = '#quantity_wanted';
      let quantityInput = $(quantityInputSelector);

      // @see http://www.virtuosoft.eu/code/bootstrap-touchspin/ about Bootstrap TouchSpin
      quantityInput.trigger('touchspin.updatesettings', {min: minimalProductQuantity});
    }
    imageScrollBox();
    $($('.tabs .nav-link.active').attr('href')).addClass('active').removeClass('fade');
    $('.js-product-images-modal').replaceWith(event.product_images_modal);
    refreshImages();
    coverImage();
  });

  function coverImage() {
    if ($(".js-qv-product-images .selected").length){
      var ind = $(".js-qv-product-images .selected").index();
      $(".js-product-images-modal .product-images li").eq(ind).find("img").addClass("selected");
    }
    $('.js-thumb').on(
      'click',
      (event) => {
        $('.js-modal-product-cover').attr('src',$(event.target).data('image-large-src'));
        $('.selected').removeClass('selected');
        $(event.target).addClass('selected');
        const index = $(event.target).parent().index();
        $(".js-product-images-modal .product-images li").eq(index).find("img").addClass("selected");
        $('.js-qv-product-cover').prop('src', $(event.currentTarget).data('image-large-src'));
      }
    );
  }

  function imageScrollBox() {
    if ($('#main .js-qv-product-images li').length > 2) {
      $('#main .js-qv-mask').addClass('scroll');
      $('.scroll-box-arrows').addClass('scroll');
        $('#main .js-qv-mask').scrollbox({
          direction: 'h',
          distance: 113,
          autoPlay: false
        });
        $('.scroll-box-arrows .left').click(function () {
          $('#main .js-qv-mask').trigger('backward');
        });
        $('.scroll-box-arrows .right').click(function () {
          $('#main .js-qv-mask').trigger('forward');
        });
    } else {
      $('#main .js-qv-mask').removeClass('scroll');
      $('.scroll-box-arrows').removeClass('scroll');
    }
  }

  function createInputFile() {
    $('.js-file-input').on('change', (event) => {
      let target, file;

      if ((target = $(event.currentTarget)[0]) && (file = target.files[0])) {
        $(target).prev().text(file.name);
      }
    });
  }

  function createProductSpin() {
    let quantityInput = $('#quantity_wanted');
    quantityInput.TouchSpin({
      verticalbuttons: true,
      verticalupclass: 'material-icons touchspin-up',
      verticaldownclass: 'material-icons touchspin-down',
      buttondown_class: 'btn btn-touchspin js-touchspin',
      buttonup_class: 'btn btn-touchspin js-touchspin',
      min: parseInt(quantityInput.attr('min'), 10),
      max: 1000000
    });

    var quantity = quantityInput.val();
    quantityInput.on('keyup change', function (event) {
      const newQuantity = $(this).val();
      if (newQuantity !== quantity) {
        quantity = newQuantity;
        let $productRefresh = $('.product-refresh');
        $(event.currentTarget).trigger('touchspin.stopspin');
        $productRefresh.trigger('click', {eventType: 'updatedProductQuantity'});
      }
      event.preventDefault();

      return false;
    });
  }
  function refreshImages(carousel) {
    var breakPoints = [3, 3, 2, 3, 2];
    var thumbs_list = $('.page-content .js-qv-product-images');

    // two columns: left or right sidebar
    if (($('body').hasClass('layout-left-column'))||($('body').hasClass('layout-right-column'))) {
      breakPoints = [2, 3, 2, 3, 2];
    }

    // one content column
    if ($('#product').hasClass('layout-full-width')) {
      breakPoints = [3, 3, 2, 3, 2];
    }

    if (thumbs_list.find('li').length > 2) {
      thumbs_list.not(".slick-slider").slick({
        slidesToShow: breakPoints[0],
        slidesToScroll: 1,
        infinite: false,
        dots: false,
        arrows: true,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" role="button" aria-disabled="false"><i class="fa fa-angle-left"></i></button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" role="button" aria-disabled="false"><i class="fa fa-angle-right"></i></button>',
        responsive: [{
          breakpoint: 1199,
          settings: {
            slidesToShow: breakPoints[1],
          }},
          {
            breakpoint: 991,
            settings: {
              slidesToShow: breakPoints[2],
            }},
          {
            breakpoint: 767,
            settings: {
              slidesToShow: breakPoints[3],
            }},
          {
            breakpoint: 575,
            settings: {
              slidesToShow: breakPoints[4],
            }}
        ]
      });
    }
  }


  function productAccessoriesCarousel() {
    var breakPoints = [2, 2, 1, 2, 1];
    var product_miniature = $('.product-accessories .products-in-carousel');
    // two columns: left or right sidebar
    if (($('body').hasClass('layout-left-column'))||($('body').hasClass('layout-right-column'))) {
      breakPoints = [3, 3, 2, 2, 1];
    }

    // one content column
    if ($('#product').hasClass('layout-full-width')) {
      breakPoints = [4, 3, 2, 2, 1];
    }

    if (product_miniature.find('li').length > 4) {
      product_miniature.not(".slick-slider").slick({
        slidesToShow: breakPoints[0],
        slidesToScroll: 1,
        infinite: false,
        dots: false,
        arrows: true,
        prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" role="button" aria-disabled="false"><i class="fa fa-angle-left"></i></button>',
        nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" role="button" aria-disabled="false"><i class="fa fa-angle-right"></i></button>',
        responsive: [{
          breakpoint: 1199,
          settings: {
            slidesToShow: breakPoints[1],
          }},
          {
            breakpoint: 991,
            settings: {
              slidesToShow: breakPoints[2],
            }},
          {
            breakpoint: 767,
            settings: {
              slidesToShow: breakPoints[3],
            }},
          {
            breakpoint: 480,
            settings: {
              slidesToShow: breakPoints[4],
            }}
        ]
      });
    }
  }
});
