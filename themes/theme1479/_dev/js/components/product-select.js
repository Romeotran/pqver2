/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import $ from 'jquery';
import 'velocity-animate/velocity.min';

export default class ProductSelect {
  constructor(prestashop){
    this.prestashop = prestashop;
  }

  init() {
    this.onProductUpdate();
    this.prestashop.on('updatedProduct', this.onProductUpdate);
  }

  onProductUpdate(){
    const MAX_THUMBS = 4;
    var $arrows =   $('.js-modal-arrows');
    var $thumbnails = $('.js-modal-product-images');
    var $product_cover = $('.js-modal-product-cover');

    $('.js-modal-thumb').on('click', (event) => {
      if($('.js-modal-thumb').hasClass('selected')){
        $('.js-modal-thumb').removeClass('selected');
      }

      $(event.currentTarget).addClass('selected');
      $product_cover.attr('src', $(event.target).data('image-large-src'));
      $product_cover.attr('title', $(event.target).attr('title'));
      $product_cover.attr('alt', $(event.target).attr('alt'));
    });

    if ($('.js-modal-product-images li').length <= MAX_THUMBS) {
      $arrows.css({'opacity': '.2', 'pointer-events': 'none', 'cursor': 'default'});
    } else {
      $arrows.on('click', (event) => {
        if ($(event.target).hasClass('arrow-up') && $thumbnails.position().top < 0) {
          this.move('up');
          $('.js-modal-arrow-down').css({'opacity':'1', 'pointer-events': 'auto', 'cursor': 'pointer'});
        } else if ($(event.target).hasClass('arrow-down') && $thumbnails.position().top + $thumbnails.height() >  $('.js-modal-mask').height()) {
          this.move('down');
          $('.js-modal-arrow-up').css({'opacity':'1', 'pointer-events': 'auto', 'cursor': 'pointer'});
        }
      });
    }
  }

  move(direction) {
    const THUMB_MARGIN = 20;
    var $thumbnails = $('.js-modal-product-images');
    var thumbHeight = $('.js-modal-product-images li').height() + THUMB_MARGIN;
    var currentPosition = $thumbnails.position().top;
    $thumbnails.velocity({
      translateY: (direction === 'up') ? currentPosition + thumbHeight : currentPosition - thumbHeight
    },function(){
      if ($thumbnails.position().top >= 0) {
        $('.js-modal-arrow-up').css({'opacity': '.2', 'pointer-events': 'none', 'cursor': 'default'});
      } else if ($thumbnails.position().top + $thumbnails.height() <=  $('.js-modal-mask').height()) {
        $('.js-modal-arrow-down').css({'opacity': '.2', 'pointer-events': 'none', 'cursor': 'default'});
      }
    });
  }
}