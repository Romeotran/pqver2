/**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
import $ from 'jquery';

$(document).ready(() => {
  var daydeal = $(".daydeal-box");
  var daydealToggle = $(".daydeal-toggle");

  responsiveResize();
  $(window).resize(responsiveResize);

  daydealToggle.on("click", function (e) {
    e.preventDefault();
    e.stopPropagation();
    var $this = $(this), isOpen = false, $box = $this.parent().find('.daydeal-box');

    if ($this.hasClass("active")){
      isOpen = true;
      $this.removeClass("active");
      $box.removeClass("active");
    }else{
      daydeal.removeClass("active");
      daydealToggle.removeClass("active");
    }


    if (!isOpen){
      $box.addClass("active");
      $this.addClass("active");
    }
  });
  $(document).on("click", function (e) {
    if (e.target != daydealToggle[0] && daydealToggle.parent().find($(e.target)).length == 0 && daydealToggle.find($(e.target)).length == 0) {
      daydealToggle.removeClass("active");
      daydeal.removeClass("active");
    }
  });
  //To Top
  $('body').append('<a id="toTop" class="toTop fa fa-angle-up" href="#"></a>');
  $(window).scroll(function () {
    var top = $('#toTop');
    if ($(this).scrollTop() > 500) {
      top.fadeIn();
    } else {
      top.fadeOut();
    }
  });
  $('#toTop').click(function (e) {
    e.preventDefault();
    $('html, body').stop().animate({
      scrollTop: 0
    }, 500)
  });
});
function initRmaItemSelector() {
  $('#order-return-form table thead input[type=checkbox]').on('click', function() {
    var checked = $(this).prop('checked');
    $('#order-return-form table tbody input[type=checkbox]').each(function(_, checkbox) {
      $(checkbox).prop('checked', checked);
    });
  });
}

function setupCustomerScripts() {
  if ($('body#order-detail')) {
    initRmaItemSelector();
  }
}

$(document).ready(setupCustomerScripts);

// Used to compensante Chrome/Safari bug (they don't care about scroll bar for width)
function scrollCompensate() {
  var inner = document.createElement('p');
  inner.style.width = '100%';
  inner.style.height = '200px';

  var outer = document.createElement('div');
  outer.style.position = 'absolute';
  outer.style.top = '0px';
  outer.style.left = '0px';
  outer.style.visibility = 'hidden';
  outer.style.width = '200px';
  outer.style.height = '150px';
  outer.style.overflow = 'hidden';
  outer.appendChild(inner);

  document.body.appendChild(outer);
  var w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  var w2 = inner.offsetWidth;

  if (w1 == w2) {
    w2 = outer.clientWidth;
  }

  document.body.removeChild(outer);

  return (w1 - w2);
}

function responsiveResize() {
  // compensante = scrollCompensate();
  if (($(window).width()+scrollCompensate()) <= 767 ) {
    tabletResize('enable');
  } else if (($(window).width()+scrollCompensate()) >= 768) {
    tabletResize('disable');
  }
}

function tabletResize(status) {
  var blockCart = $('#blockcart-wrapper');
  if (status == 'enable') {
    blockCart.appendTo($('header .blockcart-mobile'));
  } else {
    blockCart.appendTo($('header .blockcart-desktop'));
  }
}